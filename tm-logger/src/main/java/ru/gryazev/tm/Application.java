package ru.gryazev.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.gryazev.tm.configuration.ApplicationConfig;
import ru.gryazev.tm.context.Bootstrap;

import javax.jms.JMSException;

public class Application {

    public static void main( String[] args ) {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        @NotNull final Bootstrap bootstrap = context.getBean("bootstrap", Bootstrap.class);
    }

}
