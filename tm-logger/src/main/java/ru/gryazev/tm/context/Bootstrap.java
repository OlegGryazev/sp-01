package ru.gryazev.tm.context;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.listener.LogMessageListener;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;

@Component
public class Bootstrap {

    private final ApplicationContext context;

    @Autowired
    public Bootstrap(@NotNull final ApplicationContext context) throws JMSException {
        this.context = context;
        @NotNull final MessageConsumer consumer = context.getBean("consumer", MessageConsumer.class);
        consumer.setMessageListener(context.getBean("logMessageListener", LogMessageListener.class));
    }

}
