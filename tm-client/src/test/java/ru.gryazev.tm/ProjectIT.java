package ru.gryazev.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.gryazev.tm.endpoint.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.lang.Exception;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.*;

public class ProjectIT {

    @NotNull
    private static final IUserEndpoint userEndpoint = new UserEndpointService().getUserEndpointPort();

    @NotNull
    private static final ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    @NotNull
    private static final IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();

    @Nullable
    private static String token;

    @Nullable
    private static String tokenAdmin;

    @Nullable
    private static String userId;

    @Nullable
    private static String adminId;

    @BeforeClass
    public static void createTestUsers() throws Exception {
        @NotNull final User user = new User();
        user.setLogin("test");
        user.setPwdHash("4c2ee22a-3ce5-4b75-a10d-4e0daa0ffaad");
        user.setRoleType(RoleType.USER);
        user.setName("Vasya");
        userId = userEndpoint.createUser(user).getId();
        token = sessionEndpoint.createSession(user.getLogin(), user.getPwdHash());

        @NotNull final User admin = new User();
        admin.setLogin("testadmin");
        admin.setPwdHash("4c2ee22a-3ce5-4b75-a10d-4e0daa0ffaad");
        admin.setRoleType(RoleType.ADMIN);
        admin.setName("VasyaAdmin");
        adminId = userEndpoint.createUser(admin).getId();
        tokenAdmin = sessionEndpoint.createSession(admin.getLogin(), admin.getPwdHash());
    }

    @Test
    public void projectCreateOk() throws Exception {
        final int count = projectEndpoint.findProjectByUserId(token).size();
        projectEndpoint.createProject(token, getProject(userId));
        assertEquals(count + 1, projectEndpoint.findProjectByUserId(token).size());
        projectEndpoint.createProject(token, getProject(userId));
        assertEquals(count + 2, projectEndpoint.findProjectByUserId(token).size());
    }

    @Test(expected = Exception.class)
    public void projectCreateInvalidToken() throws Exception {
        projectEndpoint.createProject("InvalidToken", getProject(userId));
    }

    @Test
    public void projectEditOk() throws Exception {
        @NotNull final Project createdProject = projectEndpoint.createProject(token, getProject(userId));
        createdProject.setName("edited-project");
        projectEndpoint.editProject(token, createdProject);

        assertEquals("edited-project", projectEndpoint.findOneProject(token, createdProject.getId()).getName());
    }

    @Test
    public void projectEditInvalidId() throws Exception{
        @NotNull final Project createdProject = projectEndpoint.createProject(token, getProject(userId));
        @NotNull final String oldId = createdProject.getId();
        createdProject.setId("invalidId");
        createdProject.setName("edited-project");
        projectEndpoint.editProject(token, createdProject);
        assertNotNull(projectEndpoint.findOneProject(token, oldId));
        assertNotNull(projectEndpoint.findOneProject(token, createdProject.getId()));
    }

    @Test
    public void projectRemoveOk() throws Exception{
        final int count = projectEndpoint.findProjectByUserId(token).size();
        @NotNull final Project createdProject = projectEndpoint.createProject(token, getProject(userId));
        assertEquals(count + 1, projectEndpoint.findProjectByUserId(token).size());
        projectEndpoint.removeProject(token, createdProject.getId());
        assertEquals(count, projectEndpoint.findProjectByUserId(token).size());
    }

    @Test
    public void projectListOk() throws Exception {
        final int count = projectEndpoint.findProjectByUserId(token).size();
        projectEndpoint.createProject(token, getProject(userId));
        projectEndpoint.createProject(token, getProject(userId));
        projectEndpoint.createProject(token, getProject(userId));
        projectEndpoint.createProject(token, getProject(userId));
        projectEndpoint.createProject(token, getProject(userId));
        assertEquals(count + 5, projectEndpoint.findProjectByUserId(token).size());
    }

    @Test(expected = Exception.class)
    public void projectListAllNotAdmin() throws Exception {
        projectEndpoint.findAllProject(token);
    }

    @Test
    public void projectListAllOk() throws Exception {
        final int countUser = projectEndpoint.findProjectByUserId(token).size();
        final int countAdmin = projectEndpoint.findProjectByUserId(tokenAdmin).size();
        projectEndpoint.createProject(token, getProject(userId));
        projectEndpoint.createProject(token, getProject(userId));
        projectEndpoint.createProject(token, getProject(userId));
        projectEndpoint.createProject(tokenAdmin, getProject(adminId));
        projectEndpoint.createProject(tokenAdmin, getProject(adminId));
        projectEndpoint.createProject(tokenAdmin, getProject(adminId));
        assertEquals(countAdmin + countUser + 6, projectEndpoint.findAllProject(tokenAdmin).size());
    }

    @Test
    public void projectFindByNameOk() throws Exception {
        projectEndpoint.removeAllProject(token);
        @NotNull final Project project = getProject(userId);
        project.setName("project");
        @NotNull final Project project2 = getProject(userId);
        project2.setName("project");
        @NotNull final Project project3 = getProject(userId);
        project3.setName("project");
        @NotNull final Project project4 = getProject(userId);
        project4.setName("butItsNot");
        @NotNull final Project project5 = getProject(userId);
        project5.setName("butItsNot");
        projectEndpoint.createProject(token, project);
        projectEndpoint.createProject(token, project2);
        projectEndpoint.createProject(token, project3);
        projectEndpoint.createProject(token, project4);
        projectEndpoint.createProject(token, project5);
        assertEquals(3, projectEndpoint.findProjectByName(token, "ojec").size());
        assertEquals(2, projectEndpoint.findProjectByName(token, "ut").size());
    }

    @AfterClass
    public static void clean() throws Exception {
        userEndpoint.removeUserSelf(token);
        userEndpoint.removeUserSelf(tokenAdmin);
    }

    @Nullable
    static XMLGregorianCalendar toXmlGregorianDate(@NotNull final Date date) throws IOException {
        if (date == null) return null;
        @Nullable DatatypeFactory df = null;
        try {
            df = DatatypeFactory.newInstance();
        } catch (DatatypeConfigurationException dce) {
            throw new RuntimeException("Error during date parse");
        }
        @NotNull final GregorianCalendar gc = new GregorianCalendar();
        gc.setTimeInMillis(date.getTime());
        return df.newXMLGregorianCalendar(gc);
    }

    @NotNull
    static Project getProject(@Nullable final String userId) throws IOException {
        @NotNull final Project project = new Project();
        project.setUserId(userId);
        project.setName("project");
        project.setDetails("details-project");
        project.setDateStart(toXmlGregorianDate(new Date()));
        project.setDateFinish(toXmlGregorianDate(new Date()));
        project.setStatus(Status.PROCESSED);
        return project;
    }

}
