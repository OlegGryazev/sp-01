package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.endpoint.Status;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.endpoint.User;

import java.io.Closeable;
import java.io.IOException;

public interface ITerminalService extends Closeable {

    @NotNull
    public Task getTaskFromConsole() throws IOException;

    @NotNull
    public Project getProjectFromConsole() throws IOException;

    @NotNull
    public User getUserFromConsole() throws IOException;

    @NotNull
    public String getSearchString() throws IOException;

    @Nullable
    public User getUserPwdRepeat() throws IOException;

    @NotNull
    public String getPwdHashFromConsole() throws IOException;

    public int getTaskIndex();

    public int getProjectIndex();

    @NotNull
    public String readCommand() throws IOException;

    @Override
    public void close();

    public void print(@Nullable String str);

    @Nullable
    public Status getStatus() throws IOException;

    public void printProject(Project project);

    public void printTask(Task task);

    public void printUser(User user);

}
