package ru.gryazev.tm.bean;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class StateBean {

    @Getter
    @NotNull
    private final Map<String, String> settings = new HashMap<>();

}
