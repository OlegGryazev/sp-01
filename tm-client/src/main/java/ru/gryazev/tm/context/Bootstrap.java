package ru.gryazev.tm.context;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.gryazev.tm.api.context.SessionLocator;
import ru.gryazev.tm.api.service.ITerminalService;
import ru.gryazev.tm.command.AbstractCommand;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

public final class Bootstrap implements SessionLocator {

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ITerminalService terminalService;

    @Getter
    @Setter
    @Nullable
    private String token;

    @Getter
    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public void init() {
        terminalService.print("**** Welcome to Project Manager ****");
        context.getBeansOfType(AbstractCommand.class).forEach((k, v) -> commands.put(v.getName(), v));
        while (true) {
            try {
                @Nullable final AbstractCommand command = getCommand();
                if (command == null) continue;
                command.execute();
            } catch (Exception e) {
                terminalService.print(e.getMessage());
            }
        }
    }

    @Nullable
    private AbstractCommand getCommand() throws IOException {
        @Nullable final AbstractCommand command = commands.get(terminalService.readCommand());
        if (command == null) {
            terminalService.print("Command not found!");
            return null;
        }
        return command;
    }

}