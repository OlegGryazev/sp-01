package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.bean.StateBean;

@Component
public final class ProjectSelectCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-select";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Select project. To reset, type \"-1\".";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        final int projectIndex = terminalService.getProjectIndex();
        @Nullable final String projectId = projectEndpoint.getProjectId(token, projectIndex);
        applicationContext.getBean(StateBean.class).getSettings().put("current-project", projectId);
    }

}
