package ru.gryazev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudCreateException;
import ru.gryazev.tm.error.CrudUpdateException;

@Component
public final class UserRegistrationCommand extends AbstractCommand {

    private IUserEndpoint userEndpoint;

    public UserRegistrationCommand(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "user-registration";
    }

    @Override
    public String getDescription() {
        return "Registration of new user.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @Nullable final User user = terminalService.getUserPwdRepeat();
        if (user == null) throw new CrudUpdateException();
        @Nullable final User createdUser = userEndpoint.createUser(user);
        if (createdUser == null) throw new CrudCreateException();
        terminalService.print("[OK]");
    }

}
