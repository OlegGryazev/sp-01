package ru.gryazev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.ISessionEndpoint;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudNotFoundException;

@Component
public final class UserLoginCommand extends AbstractCommand {

    private ISessionEndpoint sessionEndpoint;

    public UserLoginCommand(@NotNull final ISessionEndpoint sessionEndpoint) {
        this.sessionEndpoint = sessionEndpoint;
        setAllowed(true);
    }

    @Override
    public String getName() {
        return "user-login";
    }

    @Override
    public String getDescription() {
        return "User authorization.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null || sessionLocator == null) return;
        @NotNull final User user = terminalService.getUserFromConsole();
        @Nullable final String token = sessionEndpoint.createSession(user.getLogin(), user.getPwdHash());
        if (token == null) throw new CrudNotFoundException();
        terminalService.print("[You are logged in]");
        sessionLocator.setToken(token);
    }

}
