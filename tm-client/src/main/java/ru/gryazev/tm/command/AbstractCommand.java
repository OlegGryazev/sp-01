package ru.gryazev.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.gryazev.tm.api.context.SessionLocator;
import ru.gryazev.tm.api.service.ITerminalService;
import ru.gryazev.tm.error.CrudInitializationException;
import ru.gryazev.tm.error.CrudUpdateException;

@Setter
@NoArgsConstructor
public abstract class AbstractCommand {

    @Autowired
    protected ApplicationContext applicationContext;

    @Nullable
    @Autowired
    protected ITerminalService terminalService;

    @Nullable
    @Autowired
    protected SessionLocator sessionLocator;

    @Getter
    private boolean allowed = false;

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute() throws Exception;

    @NotNull
    public String getToken() {
        if (sessionLocator == null) throw new CrudInitializationException();
        @Nullable final String token = sessionLocator.getToken();
        if (token == null) throw new CrudUpdateException();
        return token;
    }

}
