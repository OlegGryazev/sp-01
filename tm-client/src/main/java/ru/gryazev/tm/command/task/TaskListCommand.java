package ru.gryazev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.bean.StateBean;
import ru.gryazev.tm.endpoint.Task;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

@Component
public final class TaskListCommand extends AbstractTaskCommand {

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks of selected project.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        @Nullable final String projectId = getCurrentProjectId();
        @Nullable final String sortType = applicationContext.getBean(StateBean.class).getSettings().get("task-sort");
        @NotNull final List<Task> tasks = taskEndpoint.findTaskByProjectSorted(token, projectId, sortType);
        if (tasks.isEmpty()) throw new CrudListEmptyException();
        for (int i = 0; i < tasks.size(); i++)
            terminalService.print((i + 1) + ". " + tasks.get(i).getName());
    }

}
