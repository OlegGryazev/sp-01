package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.User;

@Component
@NoArgsConstructor
public final class UserViewCommand extends AbstractCommand {

    private IUserEndpoint userEndpoint;

    public UserViewCommand(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @Override
    public String getName() {
        return "user-view";
    }

    @Override
    public String getDescription() {
        return "View user data.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @Nullable final String token = getToken();
        @Nullable final User user = userEndpoint.findCurrentUser(token);
        if (user != null) terminalService.printUser(user);
    }

}
