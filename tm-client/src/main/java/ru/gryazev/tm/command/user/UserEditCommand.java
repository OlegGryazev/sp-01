package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.User;
import ru.gryazev.tm.error.CrudUpdateException;

@Component
@NoArgsConstructor
public final class UserEditCommand extends AbstractCommand {

    private IUserEndpoint userEndpoint;

    public UserEditCommand(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @Override
    public String getName() {
        return "user-edit";
    }

    @Override
    public String getDescription() {
        return "Edit user data.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        terminalService.print("[USER EDIT]");
        @Nullable final User userEditData = terminalService.getUserPwdRepeat();
        if (userEditData == null) return;

        @Nullable final User currentUser = userEndpoint.findCurrentUser(token);
        if (currentUser == null) throw new CrudUpdateException();
        userEditData.setRoleType(currentUser.getRoleType());
        userEditData.setId(currentUser.getId());
        @Nullable final User editedUser = userEndpoint.editUser(token, userEditData);
        if (editedUser == null) throw new CrudUpdateException();
        terminalService.print("[OK]");
    }

}
