package ru.gryazev.tm.command.task.sort;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.bean.StateBean;
import ru.gryazev.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public class TaskSortDateFinish extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-sort-date-finish";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts tasks in order of date of task finish.";
    }

    @Override
    public void execute() {
        if (terminalService == null) return;
        applicationContext.getBean(StateBean.class).getSettings().put("task-sort", "date-finish");
        terminalService.print("[OK]");
    }

}
