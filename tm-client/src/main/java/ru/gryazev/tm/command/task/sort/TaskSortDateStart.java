package ru.gryazev.tm.command.task.sort;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.bean.StateBean;
import ru.gryazev.tm.command.AbstractCommand;

@Component
@NoArgsConstructor
public class TaskSortDateStart extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "task-sort-date-start";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Sorts tasks in order of date of task start.";
    }

    @Override
    public void execute() {
        if (terminalService == null) return;
        applicationContext.getBean(StateBean.class).getSettings().put("task-sort", "date-start");
        terminalService.print("[OK]");
    }

}
