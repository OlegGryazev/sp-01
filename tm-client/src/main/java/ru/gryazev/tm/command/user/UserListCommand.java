package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IUserEndpoint;
import ru.gryazev.tm.endpoint.User;

@Component
@NoArgsConstructor
public class UserListCommand extends AbstractCommand {

    private IUserEndpoint userEndpoint;

    public UserListCommand(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @Override
    public String getName() {
        return "user-list";
    }

    @Override
    public String getDescription() {
        return "Shows all users (REQUIRES ADMIN ROLE).";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        for (@NotNull final User user : userEndpoint.findAllUser(getToken()))
            terminalService.print(user.getLogin());
    }

}
