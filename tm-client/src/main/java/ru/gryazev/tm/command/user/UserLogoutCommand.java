package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.ISessionEndpoint;

@Component
@NoArgsConstructor
public final class UserLogoutCommand extends AbstractCommand {

    private ISessionEndpoint sessionEndpoint;

    public UserLogoutCommand(@NotNull final ISessionEndpoint sessionEndpoint) {
        this.sessionEndpoint = sessionEndpoint;
    }

    @Override
    public String getName() {
        return "user-logout";
    }

    @Override
    public String getDescription() {
        return "Logout from Project Manager.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null || sessionLocator == null) return;
        @Nullable final String token = getToken();
        sessionEndpoint.removeSession(token);
        sessionLocator.setToken(null);
        terminalService.print("You are logged out.");
    }

}
