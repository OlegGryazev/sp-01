package ru.gryazev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import ru.gryazev.tm.bean.StateBean;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IProjectEndpoint;
import ru.gryazev.tm.endpoint.ITaskEndpoint;
import ru.gryazev.tm.error.CrudNotFoundException;

@NoArgsConstructor
public abstract class AbstractTaskCommand extends AbstractCommand {

    @Autowired
    protected IProjectEndpoint projectEndpoint;

    @Autowired
    protected ITaskEndpoint taskEndpoint;

    @Nullable
    public String getCurrentProjectId() {
        return applicationContext.getBean(StateBean.class).getSettings().get("current-project");
    }

    @Nullable
    public String getProjectId() throws Exception {
        if (terminalService == null) return null;
        @NotNull final String token = getToken();
        @Nullable String projectId = getCurrentProjectId();
        if (projectId == null) {
            projectId = projectEndpoint.getProjectId(token, terminalService.getProjectIndex());
        }
        if (projectId == null) throw new CrudNotFoundException();
        return projectId;
    }

}
