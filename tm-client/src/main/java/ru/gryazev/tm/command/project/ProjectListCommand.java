package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.bean.StateBean;
import ru.gryazev.tm.endpoint.Project;
import ru.gryazev.tm.error.CrudListEmptyException;

import java.util.List;

@Component
public final class ProjectListCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        @Nullable final String sortType = applicationContext.getBean(StateBean.class).getSettings().get("project-sort");
        @NotNull final List<Project> projects = projectEndpoint.findProjectByUserIdSorted(token, sortType);
        if (projects.isEmpty()) throw new CrudListEmptyException();
        for (int i = 0; i < projects.size(); i++)
            terminalService.print((i + 1) + ". " + projects.get(i).getName());
    }

}
