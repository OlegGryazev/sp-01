package ru.gryazev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Clear projects list.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null) return;
        @NotNull final String token = getToken();
        projectEndpoint.removeAllProject(token);
        terminalService.print("[CLEAR OK]");
    }

}
