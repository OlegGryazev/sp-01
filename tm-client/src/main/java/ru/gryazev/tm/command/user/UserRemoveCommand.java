package ru.gryazev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.IUserEndpoint;

@Component
@NoArgsConstructor
public class UserRemoveCommand extends AbstractCommand {

    private IUserEndpoint userEndpoint;

    public UserRemoveCommand(@NotNull final IUserEndpoint userEndpoint) {
        this.userEndpoint = userEndpoint;
    }

    @Override
    public String getName() {
        return "user-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected user (REQUIRES ADMIN ROLE).";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null || sessionLocator == null) return;
        @Nullable final String token = getToken();
        userEndpoint.removeUserSelf(token);
        sessionLocator.setToken(null);
        terminalService.print("[OK]");
    }

}
