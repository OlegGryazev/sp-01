package ru.gryazev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.command.AbstractCommand;
import ru.gryazev.tm.endpoint.ISystemEndpoint;

@Component
public class ServerInformationCommand extends AbstractCommand {

    private ISystemEndpoint systemEndpoint;

    public ServerInformationCommand(@NotNull final ISystemEndpoint systemEndpoint) {
        this.systemEndpoint = systemEndpoint;
    }

    @Override
    public String getName() {
        return "server-info";
    }

    @Override
    public String getDescription() {
        return "Shows information about server host and port.";
    }

    @Override
    public void execute() throws Exception {
        if (terminalService == null || sessionLocator == null) return;
        @Nullable final String token = getToken();
        @Nullable final String information = systemEndpoint.getServerInformation(token);
        terminalService.print(information);
    }

}
