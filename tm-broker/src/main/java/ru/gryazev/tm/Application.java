package ru.gryazev.tm;

import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;

public class Application {

    public static void main( String[] args ) throws Exception {
        @NotNull final BrokerService broker = new BrokerService();
        broker.addConnector("tcp://localhost:61616");
        broker.start();
    }

}
