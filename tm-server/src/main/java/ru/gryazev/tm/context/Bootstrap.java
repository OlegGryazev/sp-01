package ru.gryazev.tm.context;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.constant.Constant;
import ru.gryazev.tm.endpoint.*;

import javax.xml.ws.Endpoint;

@Component
public final class Bootstrap {

    @Autowired
    private ApplicationContext context;

    public void init() {
        @NotNull final String addressPattern = String.format("http://%s:%s/%s", Constant.HOST, Constant.PORT, "%s?wsdl");
        Endpoint.publish(String.format(addressPattern, "session"), context.getBean("sessionEndpoint", ISessionEndpoint.class));
        Endpoint.publish(String.format(addressPattern, "project"), context.getBean("projectEndpoint", IProjectEndpoint.class));
        Endpoint.publish(String.format(addressPattern, "task"), context.getBean("taskEndpoint", ITaskEndpoint.class));
        Endpoint.publish(String.format(addressPattern, "user"), context.getBean("userEndpoint", IUserEndpoint.class));
        Endpoint.publish(String.format(addressPattern, "system"), context.getBean("systemEndpoint", ISystemEndpoint.class));
    }

}