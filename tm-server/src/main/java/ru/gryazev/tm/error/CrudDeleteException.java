package ru.gryazev.tm.error;

public final class CrudDeleteException extends CrudException {

    public CrudDeleteException() {
        super("Error during delete operation");
    }

}
