package ru.gryazev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.util.CompareUtil;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    @Autowired
    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Nullable
    public String getProjectId(@Nullable final String userId, final int projectIndex) {
        if (userId == null || userId.isEmpty() || projectIndex < 0) return null;
        @Nullable final List<ProjectEntity> projectEntities = projectRepository.findAllByUserId(userId);
        return projectIndex >= projectEntities.size() ? null : projectEntities.get(projectIndex).getId();
    }

    @Nullable
    @Override
    public ProjectEntity create(@Nullable final String userId, @Nullable final ProjectEntity projectEntity) {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(projectEntity)) return null;
        if (!userId.equals(projectEntity.getUser().getId())) return null;
        projectRepository.persist(projectEntity);
        return projectEntity;
    }

    @Nullable
    @Override
    public ProjectEntity edit(@Nullable final String userId, @Nullable final ProjectEntity projectEntity) throws Exception {
        if (!isEntityValid(projectEntity)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(projectEntity.getUser().getId())) return null;
        projectRepository.merge(projectEntity);
        return projectEntity;
    }

    @Nullable
    @Override
    public ProjectEntity findOne(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        @Nullable final ProjectEntity projectEntity = projectRepository.findOneById(userId, projectId);
        return projectEntity;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByName(@Nullable final String userId, @Nullable final String projectName) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectName == null || projectName.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectEntity> projects = projectRepository.findAllByName(userId, projectName);
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByDetails(@Nullable final String userId, @Nullable final String projectDetails) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectDetails == null || projectDetails.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectEntity> projects = projectRepository.findAllByDetails(userId, projectDetails);
        return projects;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final List<ProjectEntity> list = projectRepository.findAllByUserId(userId);
        return list;
    }

    @NotNull
    @Override
    public List<ProjectEntity> findByUserIdSorted(@Nullable final String userId, @Nullable final String sortType) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final List<ProjectEntity> list = projectRepository.findAllByUserIdSorted(userId, sqlSortType);
        return list;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        projectRepository.removeById(userId, projectId);
    }

    @Override
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.removeAllByUserId(userId);
    }

    @Override
    public void removeAll() {
        projectRepository.removeAll();
    }

    @NotNull
    @Override
    public List<ProjectEntity> findAll() {
        @NotNull final List<ProjectEntity> projects = projectRepository.findAll();
        return projects;
    }

    private boolean isEntityValid(@Nullable final ProjectEntity projectEntity) {
        if (projectEntity == null) return false;
        if (projectEntity.getId() == null || projectEntity.getId().isEmpty()) return false;
        if (projectEntity.getName() == null || projectEntity.getName().isEmpty()) return false;
        return projectEntity.getUser().getId() != null && !projectEntity.getUser().getId().isEmpty();
    }

}
