package ru.gryazev.tm.service;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.service.ITaskService;
import ru.gryazev.tm.entity.TaskEntity;
import ru.gryazev.tm.util.CompareUtil;

import java.util.Collections;
import java.util.List;

@Service
@Transactional
public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    @Autowired
    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Nullable
    @Override
    public TaskEntity create(@Nullable final String userId, @Nullable final TaskEntity task) {
        if (userId == null || userId.isEmpty()) return null;
        if (!isEntityValid(task)) return null;
        if (!userId.equals(task.getUser().getId())) return null;
        taskRepository.persist(task);
        return task;
    }

    @Nullable
    @Override
    public TaskEntity edit(@Nullable final String userId, @Nullable final TaskEntity task) throws Exception {
        if (!isEntityValid(task)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(task.getUser().getId())) return null;
        taskRepository.merge(task);
        return task;
    }

    @Nullable
    @Override
    public TaskEntity findOne(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskId == null || taskId.isEmpty()) return null;
        @Nullable final TaskEntity task = taskRepository.findOneById(userId, taskId);
        return task;
    }

    @NotNull
    @Override
    public List<TaskEntity> listTaskUnlinked(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByUserIdUnlinked(userId);
        return tasks;
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) {
        if (userId == null || userId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        taskRepository.removeById(userId, taskId);
    }

    @Override
    public void removeByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return;
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public String getTaskId(@Nullable final String userId, @Nullable final String projectId, final int taskIndex) {
        if (userId == null || userId.isEmpty()) return null;
        if (taskIndex < 0) return null;
        @Nullable final List<TaskEntity> tasks = projectId == null ? taskRepository.findAllByUserIdUnlinked(userId) :
                taskRepository.findTasksByProjectId(userId, projectId);
        if (taskIndex >= tasks.size()) return null;
        return tasks.get(taskIndex).getId();
    }

    @NotNull
    @Override
    public List<TaskEntity> findByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskEntity> tasks = taskRepository.findTasksByProjectId(userId, projectId);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> findByName(@Nullable final String userId, @Nullable final String taskName) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskName == null || taskName.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByName(userId, taskName);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> findByDetails(@Nullable final String userId, @Nullable final String taskDetails) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (taskDetails == null || taskDetails.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByDetails(userId, taskDetails);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> listTaskByProjectSorted(
            @Nullable String userId,
            @Nullable String projectId,
            @Nullable String sortType
    ) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByProjectIdSorted(userId, projectId, sqlSortType);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> listTaskUnlinkedSorted(@Nullable String userId, @Nullable String sortType) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final String sqlSortType = CompareUtil.getSqlSortType(sortType);
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByUserIdUnlinkedSorted(userId, sqlSortType);
        return tasks;
    }

    @NotNull
    @Override
    public List<TaskEntity> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        @NotNull final List<TaskEntity> tasks = taskRepository.findAllByUserId(userId);
        return tasks;
    }

    @Override
    public void removeAll() {
        taskRepository.removeAll();
    }

    @NotNull
    @Override
    public List<TaskEntity> findAll() {
        @NotNull final List<TaskEntity> tasks = taskRepository.findAll();
        return tasks;
    }

    @Contract("null -> false")
    public boolean isEntityValid(@Nullable final TaskEntity task) {
        if (task == null) return false;
        if (task.getId() == null || task.getId().isEmpty()) return false;
        if (task.getUser() == null || task.getUser().getId() == null || task.getUser().getId().isEmpty()) return false;
        return (task.getName() != null && !task.getName().isEmpty());
    }

}