package ru.gryazev.tm.service;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;

import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class UserService implements IUserService {

    private final IUserRepository userRepository;

    @Autowired
    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Nullable
    @Override
    public UserEntity create(@Nullable final UserEntity userEntity) {
        if (!isEntityValid(userEntity)) return null;
        userRepository.persist(userEntity);
        return userEntity;
    }

    public boolean checkRole(@Nullable final String userId, @Nullable final RoleType[] roles) {
        if (roles == null) return false;
        if (userId == null || userId.isEmpty()) return false;
        @Nullable final UserEntity userEntity = userRepository.findOneById(userId);
        if (userEntity == null) return false;
        return Arrays.asList(roles).contains(userEntity.getRoleType());
    }

    @Nullable
    @Override
    public String getUserId(int userIndex) {
        if (userIndex < 0) return null;
        @NotNull final List<UserEntity> userEntities = userRepository.findAll();
        return userIndex >= userEntities.size() ? null : userEntities.get(userIndex).getId();
    }

    @Contract("null -> false")
    public boolean isEntityValid(@Nullable final UserEntity userEntity) {
        if (userEntity == null || userEntity.getRoleType() == null) return false;
        if (userEntity.getId().isEmpty()) return false;
        if (userEntity.getLogin() == null || userEntity.getLogin().isEmpty()) return false;
        return userEntity.getPwdHash() != null && !userEntity.getPwdHash().isEmpty();
    }

    @Nullable
    @Override
    public UserEntity findOne(@Nullable final String id) {
        if (id == null || id.isEmpty()) return null;
        @Nullable final UserEntity userEntity = userRepository.findOneById(id);
        return userEntity;
    }

    @Nullable
    @Override
    public UserEntity edit(@Nullable final String userId, @Nullable final UserEntity userEntity) throws Exception {
        if (!isEntityValid(userEntity)) return null;
        if (userId == null || userId.isEmpty()) return null;
        if (!userId.equals(userEntity.getId())) return null;
        userRepository.merge(userEntity);
        return userEntity;
    }

    @Override
    public void remove(@Nullable final String id) {
        if (id == null || id.isEmpty()) return;
        userRepository.removeAllByUserId(id);
    }

    @Override
    public void removeAll() {
        userRepository.removeAll();
    }

    @NotNull
    @Override
    public List<UserEntity> findAll() {
        @NotNull final List<UserEntity> userEntities = userRepository.findAll();
        return userEntities;
    }

}
