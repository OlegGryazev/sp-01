package ru.gryazev.tm.entity;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.dto.*;

public class DtoFactory {

    public static <T extends AbstractCrudDTO> T convertEntityToDto(AbstractCrudEntity entity) {
        if (entity == null) return null;
        if (entity instanceof ProjectEntity) return (T) convertToProjectDto(entity);
        if (entity instanceof TaskEntity) return (T) convertToTaskDto(entity);
        if (entity instanceof UserEntity) return (T) convertToUserDto(entity);
        if (entity instanceof SessionEntity) return (T) convertToSessionDto(entity);
        return null;
    }

    @NotNull
    private static Project convertToProjectDto(@NotNull final AbstractCrudEntity entity) {
        @NotNull final ProjectEntity projectEntity = (ProjectEntity) entity;
        @NotNull final Project project = new Project();
        project.setId(projectEntity.getId());
        project.setName(projectEntity.getName());
        project.setDetails(projectEntity.getDetails());
        project.setDateStart(projectEntity.getDateStart());
        project.setDateFinish(projectEntity.getDateFinish());
        project.setStatus(projectEntity.getStatus());
        project.setUserId(projectEntity.getUser().getId());
        project.setCreateMillis(projectEntity.getCreateMillis());
        return project;
    }

    @NotNull
    private static Task convertToTaskDto(@NotNull final AbstractCrudEntity entity) {
        @NotNull final TaskEntity taskEntity = (TaskEntity) entity;
        @NotNull final Task task = new Task();
        task.setId(taskEntity.getId());
        task.setProjectId(taskEntity.getProject() == null ? null : taskEntity.getProject().getId());
        task.setName(taskEntity.getName());
        task.setDetails(taskEntity.getDetails());
        task.setDateStart(taskEntity.getDateStart());
        task.setDateFinish(taskEntity.getDateFinish());
        task.setStatus(taskEntity.getStatus());
        task.setUserId(taskEntity.getUser().getId());
        task.setCreateMillis(taskEntity.getCreateMillis());
        return task;
    }

    @NotNull
    private static Session convertToSessionDto(@NotNull final AbstractCrudEntity entity) {
        @NotNull final SessionEntity sessionEntity = (SessionEntity) entity;
        @NotNull final Session session = new Session();
        session.setId(sessionEntity.getId());
        session.setSignature(sessionEntity.getSignature());
        session.setUserId(sessionEntity.getUser().getId());
        session.setRole(sessionEntity.getRole());
        session.setTimestamp(sessionEntity.getTimestamp());
        return session;
    }

    @NotNull
    private static User convertToUserDto(@NotNull final AbstractCrudEntity entity) {
        @NotNull final UserEntity userEntity = (UserEntity) entity;
        @NotNull final User user = new User();
        user.setId(userEntity.getId());
        user.setName(userEntity.getName());
        user.setLogin(userEntity.getLogin());
        user.setPwdHash(userEntity.getPwdHash());
        user.setRoleType(userEntity.getRoleType());
        return user;
    }

}
