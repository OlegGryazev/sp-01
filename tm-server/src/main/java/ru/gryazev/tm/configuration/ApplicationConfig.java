package ru.gryazev.tm.configuration;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.*;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.api.repository.IUserRepository;
import ru.gryazev.tm.api.service.*;
import ru.gryazev.tm.context.Bootstrap;
import ru.gryazev.tm.endpoint.*;
import ru.gryazev.tm.repository.ProjectRepository;
import ru.gryazev.tm.repository.SessionRepository;
import ru.gryazev.tm.repository.TaskRepository;
import ru.gryazev.tm.repository.UserRepository;
import ru.gryazev.tm.service.*;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@Configuration
@Import({PersistenceJPAConfig.class})
@ComponentScan(basePackages = "ru.gryazev.tm")
public class ApplicationConfig {

    @Bean
    public ActiveMQConnectionFactory activeMQConnectionFactory() {
        return new ActiveMQConnectionFactory();
    }

    @Bean
    public Connection activeMQConnection(@NotNull final ActiveMQConnectionFactory activeMQConnectionFactory) throws JMSException {
        @NotNull final Connection connection = activeMQConnectionFactory.createConnection();
        connection.start();
        return connection;
    }

}
