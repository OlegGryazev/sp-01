package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.api.repository.ISessionRepository;
import ru.gryazev.tm.entity.SessionEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class SessionRepository implements ISessionRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Nullable
    public SessionEntity findOneById(@NotNull final String id) {
        @NotNull final TypedQuery<SessionEntity> query = entityManager
                .createQuery("SELECT s FROM SessionEntity s WHERE s.id = :id", SessionEntity.class)
                .setHint("org.hibernate.cacheable", Boolean.TRUE)
                .setParameter("id", id);
        @NotNull final List<SessionEntity> resultList = query.getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    public void persist(@NotNull final SessionEntity sessionEntity) {
        entityManager.persist(sessionEntity);
    }

    public void removeById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    public void removeAllByUserId(@NotNull final String userId){
        entityManager.createQuery("DELETE FROM SessionEntity s WHERE s.user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void merge(@NotNull final SessionEntity sessionEntity) throws Exception {
        throw new Exception("Session edit is not allowed.");
    }

    @NotNull
    public List<SessionEntity> findAll() {
        return entityManager.createQuery("SELECT s FROM SessionEntity s", SessionEntity.class).getResultList();
    }

    @NotNull
    public List<SessionEntity> findByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT s FROM SessionEntity s " +
                "WHERE user.id = :userId", SessionEntity.class)
                .setHint("org.hibernate.cacheable", Boolean.TRUE)
                .setParameter("userId", userId).getResultList();
    }

    public void removeAll() {
        entityManager.createQuery("DELETE FROM SessionEntity").executeUpdate();
    }

}
