package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.api.repository.ITaskRepository;
import ru.gryazev.tm.entity.TaskEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
public class TaskRepository implements ITaskRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Nullable
    public TaskEntity findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final TypedQuery<TaskEntity> query = entityManager.createQuery("SELECT t FROM TaskEntity t " +
                "WHERE user.id = :userId AND id = :id", TaskEntity.class)
                .setHint("org.hibernate.cacheable", Boolean.TRUE)
                .setParameter("userId", userId).setParameter("id", id);
        @NotNull final List<TaskEntity> resultList = query.getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    public void persist(@NotNull final TaskEntity taskEntity) {
        entityManager.persist(taskEntity);
    }

    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    public void removeAllByUserId(@NotNull final String userId) {
        for (final TaskEntity taskEntity : findAllByUserId(userId)) entityManager.remove(taskEntity);
    }

    public void merge(@NotNull final TaskEntity taskEntity) {
        entityManager.merge(taskEntity);
    }

    @NotNull
    public List<TaskEntity> findAll() {
        return entityManager.createQuery("SELECT t FROM TaskEntity t ORDER BY t.createMillis", TaskEntity.class).getResultList();
    }

    public void removeAll() {
        for (final TaskEntity taskEntity : findAll()) entityManager.remove(taskEntity);
    }

    @NotNull
    public List<TaskEntity> findAllByName(@NotNull final String userId, @NotNull final String taskName) {
        return entityManager.createQuery("SELECT t FROM TaskEntity t " +
                "WHERE t.user.id = :userId AND name LIKE :name ORDER BY t.createMillis", TaskEntity.class)
                .setParameter("userId", userId).setParameter("name", "%" + taskName + "%").getResultList();
    }

    @NotNull
    public List<TaskEntity> findAllByDetails(@NotNull final String userId, @NotNull final String taskDetails) {
        return entityManager.createQuery("SELECT t FROM TaskEntity t " +
                "WHERE t.user.id = :userId AND details LIKE :details ORDER BY t.createMillis", TaskEntity.class)
                .setParameter("userId", userId).setParameter("details", "%" + taskDetails + "%").getResultList();
    }

    @NotNull
    public List<TaskEntity> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskEntity t " +
                "WHERE t.user.id = :userId ORDER BY t.createMillis", TaskEntity.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    public List<TaskEntity> findAllByProjectIdSorted(
            @NotNull final String userId,
            @NotNull final String projectId,
            @NotNull final String sqlSortType
    ) {
        @NotNull final String query = "SELECT t FROM TaskEntity t " +
                "WHERE t.user.id = :userId AND project.id = :projectId ORDER BY " + sqlSortType;
        return entityManager.createQuery(query, TaskEntity.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    public List<TaskEntity> findAllByUserIdUnlinked(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskEntity t " +
                "WHERE t.user.id = :userId AND project.id IS NULL ORDER BY t.createMillis", TaskEntity.class)
                .setParameter("userId", userId).getResultList();
    }

    public void removeByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        for (final TaskEntity taskEntity : findTasksByProjectId(userId, projectId))
            entityManager.remove(taskEntity);
    }

    @NotNull
    public List<TaskEntity> findTasksByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT t FROM TaskEntity t " +
                "WHERE t.user.id = :userId AND project.id = :projectId ORDER BY t.createMillis", TaskEntity.class)
                .setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }

    @NotNull
    public List<TaskEntity> findAllByUserIdUnlinkedSorted(@NotNull final String userId, @NotNull final String sqlSortType) {
        @NotNull final String query = "SELECT t FROM TaskEntity t " +
                "WHERE t.user.id = :userId AND project.id IS NULL ORDER BY " + sqlSortType;
        return entityManager.createQuery(query, TaskEntity.class)
                .setParameter("userId", userId).getResultList();
    }

}
