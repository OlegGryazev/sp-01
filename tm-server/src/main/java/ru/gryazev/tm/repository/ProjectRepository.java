package ru.gryazev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.gryazev.tm.api.repository.IProjectRepository;
import ru.gryazev.tm.entity.ProjectEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class ProjectRepository implements IProjectRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Nullable
    public ProjectEntity findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final TypedQuery<ProjectEntity> query = entityManager
                .createQuery("SELECT p FROM ProjectEntity p WHERE user.id = :userId AND id = :id", ProjectEntity.class)
                .setHint("org.hibernate.cacheable", Boolean.TRUE)
                .setParameter("userId", userId).setParameter("id", id);
        @NotNull final List<ProjectEntity> resultList = query.getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    public void persist(@NotNull final ProjectEntity projectEntity) {
        entityManager.persist(projectEntity);
    }

    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    public void removeAllByUserId(@NotNull final String userId) {
        for (final ProjectEntity projectEntity : findAllByUserId(userId)) entityManager.remove(projectEntity);
    }

    public void merge(@NotNull final ProjectEntity projectEntity) {
        entityManager.merge(projectEntity);
    }

    @NotNull
    public List<ProjectEntity> findAll() {
        return entityManager.createQuery("SELECT p FROM ProjectEntity p ORDER BY p.createMillis", ProjectEntity.class).getResultList();
    }

    public void removeAll() {
        for (final ProjectEntity projectEntity : findAll()) entityManager.remove(projectEntity);
    }

    @NotNull
    public List<ProjectEntity> findAllByName(@NotNull final String userId, @NotNull final String projectName) {
        return entityManager.createQuery("SELECT p FROM ProjectEntity p WHERE user.id = :userId AND name LIKE :projectName ORDER BY p.createMillis", ProjectEntity.class)
                .setParameter("userId", userId).setParameter("projectName", "%" + projectName + "%").getResultList();
    }

    @NotNull
    public List<ProjectEntity> findAllByDetails(@NotNull final String userId, @NotNull final String projectDetails) {
        return entityManager.createQuery("SELECT p FROM ProjectEntity p WHERE user.id = :userId AND details LIKE :projectDetails ORDER BY p.createMillis", ProjectEntity.class)
                .setParameter("userId", userId).setParameter("projectDetails", "%" + projectDetails + "%").getResultList();
    }

    @NotNull
    public List<ProjectEntity> findAllByUserId(@NotNull final String userId) {
        return entityManager.createQuery("SELECT p FROM ProjectEntity p WHERE user.id = :userId ORDER BY p.createMillis", ProjectEntity.class)
                .setParameter("userId", userId).getResultList();
    }

    @NotNull
    public List<ProjectEntity> findAllByUserIdSorted(@NotNull final String userId, @NotNull final String sqlSortType) {
        @NotNull final String query = "SELECT p FROM ProjectEntity p WHERE user.id = :userId ORDER BY " + sqlSortType;
        return entityManager.createQuery(query, ProjectEntity.class)
                .setParameter("userId", userId).getResultList();
    }

}
