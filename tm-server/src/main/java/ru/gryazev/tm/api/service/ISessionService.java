package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.entity.SessionEntity;
import ru.gryazev.tm.enumerated.RoleType;

import java.util.List;

public interface ISessionService {

    @Nullable
    public String create(@Nullable String login, @Nullable String pwdHash) throws Exception;

    public void removeAll() throws Exception;

    public void remove(@Nullable final String id) throws Exception;

    @NotNull
    List<SessionEntity> findByUserId(@Nullable final String userId);

    @Contract("null -> fail")
    public void validateSession(@Nullable Session session) throws Exception;

    @Contract("null -> fail")
    public void validateSession(@Nullable Session session, @Nullable RoleType[] roles) throws Exception;

}
