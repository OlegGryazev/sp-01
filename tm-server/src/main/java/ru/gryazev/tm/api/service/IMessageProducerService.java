package ru.gryazev.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;

public interface IMessageProducerService {

    public void sendMessage(@NotNull String message) throws JMSException;

}
