package ru.gryazev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.gryazev.tm.entity.SessionEntity;

import javax.persistence.EntityManager;
import java.util.List;

public interface ISessionRepository extends IRepository<SessionEntity> {

    public SessionEntity findOneById(@NotNull String id);

    public void removeById(@NotNull String id);

    @NotNull
    public List<SessionEntity> findByUserId(@NotNull final String userId);

}
