package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.api.service.IProjectService;
import ru.gryazev.tm.dto.Project;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.entity.DtoFactory;
import ru.gryazev.tm.entity.ProjectEntity;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@WebService(endpointInterface = "ru.gryazev.tm.endpoint.IProjectEndpoint")
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {
    
    @Autowired
    private IProjectService projectService;
    
    @NotNull
    @Override
    public List<Project> findAllProject(@Nullable final String token) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session, new RoleType[]{RoleType.ADMIN});
        @NotNull final List<ProjectEntity> entities = projectService.findAll();
        @NotNull final List<Project> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @Nullable
    @Override
    public Project createProject(@Nullable final String token, @Nullable final Project project) throws Exception {
        if (token == null || project == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @Nullable final ProjectEntity createdProjectEntity = projectService
                .create(session.getUserId(), Project.toProjectEntity(context, project));
        return DtoFactory.convertEntityToDto(createdProjectEntity);
    }

    @Nullable
    @Override
    public String getProjectId(@Nullable final String token, final int projectIndex) throws Exception {
        if (token == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        return projectService.getProjectId(session.getUserId(), projectIndex);
    }

    @Nullable
    @Override
    public Project findOneProject(@Nullable final String token, @Nullable final String entityId) throws Exception {
        if (token == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @Nullable final ProjectEntity projectEntity = projectService
                .findOne(session.getUserId(), entityId);
        return DtoFactory.convertEntityToDto(projectEntity);
    }

    @NotNull
    @Override
    public List<Project> findProjectByUserId(@Nullable final String token) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @NotNull final List<ProjectEntity> entities = projectService.findByUserId(session.getUserId());
        @NotNull final List<Project> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @Override
    public @NotNull List<Project> findProjectByUserIdSorted(@Nullable String token, @Nullable String sortType) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @NotNull final List<ProjectEntity> entities = projectService.findByUserIdSorted(session.getUserId(), sortType);
        @NotNull final List<Project> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @Nullable
    @Override
    public Project editProject(@Nullable final String token, @Nullable final Project project) throws Exception {
        if (token == null || project == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @Nullable final ProjectEntity editedProjectEntity = projectService
                .edit(session.getUserId(), Project.toProjectEntity(context, project));
        return DtoFactory.convertEntityToDto(editedProjectEntity);
    }

    @NotNull
    @Override
    public List<Project> findProjectByDetails(@Nullable String token, @NotNull String projectDetails) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @NotNull final List<ProjectEntity> entities = projectService.findByDetails(session.getUserId(), projectDetails);
        @NotNull final List<Project> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @NotNull
    @Override
    public List<Project> findProjectByName(@Nullable String token, @Nullable String projectName) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @NotNull final List<ProjectEntity> entities = projectService.findByName(session.getUserId(), projectName);
        @NotNull final List<Project> dtos = new ArrayList<>();
        entities.forEach(o -> dtos.add(DtoFactory.convertEntityToDto(o)));
        return dtos;
    }

    @Override
    public void removeProject(@Nullable final String token, @Nullable final String entityId) throws Exception {
        if (token == null) return;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        projectService.remove(session.getUserId(), entityId);
    }

    @Override
    public void removeAllProject(@Nullable final String token) throws Exception {
        if (token == null) return;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        projectService.removeAll(session.getUserId());
    }
    
}
