package ru.gryazev.tm.endpoint;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.PropertySource;
import ru.gryazev.tm.api.service.ISessionService;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.util.CryptUtil;

import javax.jws.WebMethod;

@PropertySource("server.properties")
public abstract class AbstractEndpoint {

    @Autowired
    protected ApplicationContext context;

    @Autowired
    protected ISessionService sessionService;

    @Value("${key}")
    private String key;

    @NotNull
    @WebMethod(exclude = true)
    Session getSessionFromToken(@NotNull final String token) throws Exception {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = CryptUtil.decrypt(token, key);
        @NotNull final Session session = objectMapper.readValue(json, Session.class);
        return session;
    }

}
