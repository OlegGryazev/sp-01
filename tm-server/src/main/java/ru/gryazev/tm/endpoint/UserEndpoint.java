package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.gryazev.tm.api.service.IUserService;
import ru.gryazev.tm.dto.Session;
import ru.gryazev.tm.dto.User;
import ru.gryazev.tm.entity.DtoFactory;
import ru.gryazev.tm.entity.UserEntity;
import ru.gryazev.tm.enumerated.RoleType;

import javax.jws.WebService;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Component
@WebService(endpointInterface = "ru.gryazev.tm.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    @Autowired
    private IUserService userService;

    @Nullable
    public User findOneUser(@Nullable final String token, @Nullable final String userId) throws Exception {
        if (token == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session, new RoleType[]{RoleType.ADMIN});
        @Nullable final UserEntity userEntity = userService.findOne(userId);
        return DtoFactory.convertEntityToDto(userEntity);
    }

    @Nullable
    public User findCurrentUser(@Nullable final String token) throws Exception {
        if (token == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @Nullable final UserEntity userEntity = userService.findOne(session.getUserId());
        return DtoFactory.convertEntityToDto(userEntity);
    }

    @Nullable
    @Override
    public User createUser(@Nullable User user) throws Exception {
        if (user == null) return null;
        @Nullable final UserEntity userEntity = User.toUserEntity(context, user);
        @Nullable final UserEntity createdUserEntity = userService.create(userEntity);
        return DtoFactory.convertEntityToDto(createdUserEntity);
    }

    @Nullable
    @Override
    public User editUser(@Nullable final String token, @Nullable final User user) throws Exception {
        if (token == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        @Nullable final UserEntity editedUserEntity = userService
                .edit(session.getUserId(), User.toUserEntity(context, user));
        return DtoFactory.convertEntityToDto(editedUserEntity);
    }

    @NotNull
    @Override
    public List<User> findAllUser(@Nullable String token) throws Exception {
        if (token == null) return Collections.emptyList();
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session, new RoleType[]{RoleType.ADMIN});
        @NotNull final List<User> users = new ArrayList<>();
        userService.findAll().forEach(o -> users.add(DtoFactory.convertEntityToDto(o)));
        return users;
    }

    @Override
    public void removeUserSelf(@Nullable String token) throws Exception {
        if (token == null) return;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        userService.remove(session.getUserId());
    }

    @Nullable
    @Override
    public String getUserId(@Nullable String token, int userIndex) throws Exception {
        if (token == null) return null;
        @NotNull final Session session = getSessionFromToken(token);
        sessionService.validateSession(session);
        return userService.getUserId(userIndex);
    }
    
}
