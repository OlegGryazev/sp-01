package ru.gryazev.tm.endpoint;

import org.jetbrains.annotations.Nullable;

import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    public void removeSession(@Nullable String token) throws Exception;

    @Nullable
    public String createSession(@Nullable String login, @Nullable String pwdHash) throws Exception;

}
